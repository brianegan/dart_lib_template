# gitlab_dart_lib
[![build status](https://gitlab.com/brianegan/YOUR+PROJECT+HERE/badges/master/build.svg)](https://gitlab.com/brianegan/YOUR+PROJECT+HERE/commits/master)  [![coverage report](https://gitlab.com/brianegan/YOUR+PROJECT+HERE/badges/master/coverage.svg)](https://brianegan.gitlab.io/YOUR+PROJECT+HERE/coverage/)

A library for Dart developers. It is awesome.

## Usage

A simple usage example:

    import 'package:gitlab_dart_lib/gitlab_dart_lib.dart';

    main() {
      var awesome = new Awesome();
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
